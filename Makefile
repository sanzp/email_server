.PHONY: build

build:
		packer build packer/mail.json
run:
		docker run --privileged -v /sys/fs/cgroup:/sys/fs/cgroup -d --name mail -it forex/mail:0.1 "bash"
stop:
		docker kill mail
clean:
		docker kill mail ; docker rm mail
