###Ansible playbooks for setup and provisioning Email server based on postfix###
Ansible playbooks for setup, provisioning and move configs from production Email server. for testing playbooks using packer + docker

##Prerequisites##

* ansible
* packer
* docker

##Usage##

* **make build** --build docker image and run ansible playbooks
* **make run** -- run docker container from built image
* **make stop** -- stop container
* **make clean** -- stop and remove image

##Usage instructions##

* email.yml use for install and copy configuration file to new servers
* webservers.yml use fot copy file for webmail.taxesforexpats.com site
* virtualmin.yml install virtualmin file
* rsync.yml copy taxesfor dir to new server.

* To work, you must to change the settings:
 * - roles/rsync/files/rsync.sh change variables: 
 home_serv=''
 path_key=''

* - roles/webserver/vars/main.yml - Change setting for httpd

* - inventory - Write access to a new server